---
title: |
  kbin Mod Tools and Spam Suppression Brain Dump and Road Map
---
# White Paper - /kbin Mod Tools and Mangment of Rougue Content


**Table of Contents**

[1. Introduction](#introduction)

[2. Identification of 'Best Practice'](#identification-of-best-practice)

[3. The Vision, Lines of Defence, and Roadmap](#the-vision-lines-of-defence-and-roadmap)

[4. Roles of Administrators, Magazine Owners and Moderators](#roles-of-administrators-magazine-owners-and-moderators)

[4.1. Admin](#admin)

[4.2. Magazine Owners](#magazine-owners)

[4.3. Magazine Moderators](#magazine-moderators)

[4.4. Users](#users)

[5. Reporting Tools](#reporting-tools)

[6. Federation](#federation)

[6.1. Federation of Mod Actions](#federation-of-mod-actions)

[6.2. Instance Federation](#__RefHeading___Toc497_3009672703)

[6.3. Moderators of Federated Magazines](#moderators-of-federated-magazines)

[7. API](#api)

[8. Heuristic assessment of user](#heuristic-assessment-of-user)

[9. Content and Image Scanning](#content-and-image-scanning)

[10. Problem areas](#problem-areas)

[11. Mod Action Authority Levels Table](#mod-action-authority-levels-table)

# Introduction

This document seeks to set out the ambition and road map to achieve a
best-in-class set of moderation and spam suppression tools as part of
the kbin platform giving admins and magazine owners the confidence they
have the tools they need to mange and effectively share and delegate the
task of moderation of posts made to their platforms.

The need for effective moderation tools should be self-evident.
Magazines need to be able to protect their identities, and advertisement
and excessive self promotion is generally not welcomed and, more than
that, we need to protect our end users -- links to nefarious websites
potentially containing malware or phishing need to be removed in a
timely manor and finally but mostly importantly the possibility exists
for material such as CSAM to be posted to the platform, a this point
there is then a legal and moral imperative to ensure such events can be
dealt with swiftly and effectively.

Some of the downsides which may result from ineffective moderation are
perhaps more pronounced for kbin than other comparable platforms. Unlike
propitiatory platforms (e.g. Reddit) kbin exists in the fediverse
meaning any unwanted content posted will also federated out to other
instances amplifying the original issue, and, unlike some of the larger
players in the fediverse (e.g. Mastodon) the potential reach for a post
by a bad actor is great because any user can post to even the most
popular magazines.

# Identification of 'Best Practice'

FEEDBACK NEEDED

There is a view prevailing view that currently (September 23) Lemmy has
slightly better Mod tools but that, there too, these tools are
insufficient.

Mastodon has more granular and flexible tools around different types of
federation to allow some content management at that level.

With respect to Reddit, it is often suggested that for the mod tools in
3^rd^ party apps were better than Reddit's own app.

# The Vision, Lines of Defence, and Roadmap

It is probable that no single solution for moderation exists, and a
number of vectors will need to be progressed in parallel in order to
both individually, and in combination, help mange and reduce rogue
content.

Areas for work and consideration include:

-   More levels and more flexibility around who can perform moderation
    tasks

-   Development of better tools for magazine moderators

-   Review and refinement of what powers moderators have, and what they
    can and cannot see.

-   Enable more users to develop moderation tools and implement these
    via API

-   Develop heuristic and automated moderation tools within the kbin
    core

# Roles of Administrators, Magazine Owners and Moderators

## Admin

Each kbin instance has only a small number of administrators; often only one. This is a potential pinch point since only this individual is able to perform many key moderation tasks. Clearly no single individual can be on hand 24h / day 365 days / year.

Only an administrator can ban users from the instance, or manage content on a magazine where they are not a moderator.

## Magazine Owners

Each magazine currently has one owner -- this individual has the power
to promote users to moderators, or delete the mod status from users.

The magazine owner is also a moderator.

## Magazine Moderators

Each moderator can

-   Tag posts and comments as NSFW,

-   Delete posts and comments

-   Ban users from interacting with the magazine

## Users

Users can report individual posts, they can't currently report users(?)

Currently there is no appeal process.

# Reporting Tools

Thought probably needs to be given to what tools, reporting and
notifications are needed to allow users to alert moderators and admins
of when and where action is needed.

These could range from a notification to a moderator, or moderators for
any post in a magazine (which might be useful for a generally quiet
magazine), notifications for user driven reports (already implemented),
notifications for any auto-mod actions (although that might be a
function to be undertaken by the auto-mod tool), or activity spike
notifications.

# Federation 

## Federation of Mod Actions

In principle when a post is moderated on the parent instance, this
action is pushed to all federated instances. This should be supported
and it is a priority that this happens as reliably and robustly as
possible.

> *Blue Sky Thinking*
>
> *Potentially in the future investigate a 'moderation request' being
> shared back to parent instance if a post is moderated in a federated
> instance. This isn't currently part of Activitypub thus a bespoke
> solution would need to be developed which would likely only be
> supported between native kbin instances initially.*

## Instance Federation

Kbin grants individual users significant flexibility to block users and
instances and even domains. This is good, and more powerful than the
tools available to users of other platforms (e.g. Lemmy).

At an instance level the tools available to instance administrators are
are more limited.

FEEDBACK NEEDED

More granular tools to limit federation with rogue instances are needed.

A good staring point might be the levels of federation or restriction
facilitated by Mastodon.

<https://docs.joinmastodon.org/admin/moderation/>

## Moderators of Federated Magazines

Currently the instance admin becomes the owner and only moderator of any
federated magazines. In theory the parent instance is responsible for
moderation of content in that magazine, however some instances and some
administrators will want to enforce more or less strict moderation
rules.

We should consider having a default list of user accounts which become
the moderators of any federated magazines -- this could be a mixture
real users, and auto-moderation bots.

# API

The API is not directly a moderation tool, however this is an enabler.
It is also potentially a double edged sword. The API will potentially
make it easier for bad actors to automate generation of spam posts.
However the API will also allow 3^rd^ parties to develop tools to help
them moderate their own magazines -- it will also give them far more
flexibility to moderate in the way they want to, this might include
enforcing magazine simple specific rules, but the API will also allow
users to develop more sophisticated moderation tools using the language
and tools they prefer. It is note worthy that moderation bots were
historically one of the key uses for the API on reddit with several
auto-moderation bots having been developed by the community.

There may be merit to developing a very simple demonstration of a
moderation bot using the API and then providing and advertising this
this to kbin end users, trying to be proactive in encouraging a wide
variety of users to develop moderation tools and scripts.

# Heuristic assessment of user

It is intuitive that some checks are performed on the user when new
posts are made.

At the simplest level if a large volume of similar posts with hyperlinks
have been created in a short period of time such activity might be
flagged as suspicious and perhaps restricted (indeed some rate limiting
is already implemented).

Rules could be coded into kbin's core code, with options for
administrator to adjust parameters and vary the sensitivity levels.

If using a heuristic algorithm to auto moderate we probably also need a
suitable appeal process.

> *Blue Sky Thinking*
>
> *The existence of the Moderation Log may mean there is potential for a
> 'Machine Learning' type algorithm to be developed to as a first line
> defence auto-mod tool.*


> *Blue Sky Thinking*
>
> *Another option worthy of consideration is perhaps to facilitate use
> of a closed source plug-in which would provide the heuristic
> assessment. The kbin development team would likely still need to
> develop the heuristic assessment tool, but limiting access to a number
> of more trusted developers might make it more difficult for bad actors
> to design tools to bypass the heuristic tests.*

# Content and Image Scanning

The user [\@db0@lemmy.dbzer0.com](https://lemmy.dbzer0.com/u/db0)
recently announced that he has developed a tool using AI to scan images
and detect CASM before it is posted to Lemmy instances.

See:

<https://kbin.social/m/div0@lemmy.dbzer0.com/t/471735/I-just-developed-and-deployed-the-first-real-time-protection-for>

The tool is FOSS, we might seek to adapt this tool to also work on kbin
instances, or to develop something similar.

Worth noting the developer also seems to be a advocate of FOSS in
general, he may be willing to support implementing this tool within
kbin.

Potentially other types of posts could also be scanned before posts are
allows, for example a url blacklisting -- however this may have very
limited benefit given the ease with which domains can be registered.

# Problem areas 

-   An example of an potentially problematic situation is that currently
    if a user "User1" blocks another user "BadActor" as themselves.
    Let's say User1 is also a moderator of one or more magazines -
    BadActor could currently idenitify the magazine(s) which User1
    moderates, and post liberally to them. User1 would not be able to
    see the new and potentially undesirable content which has been
    posted and therefore would not moderate it.

-   Currently many tasks can only be performed by the instance admin,
    and there can only be one admin.

-   Ideally there should be some level of cover 24h / day, 356 days /
    year

# Mod Action Authority Levels Table

The table below outlines some example moderation tasks, and gives an
indication of the who could perform the task

  
  |                           |***Admin***  |***Magazine Owner***|***Moderator*** |***Normal User***|***Comment***|
  |---------------------------|-------------|--------------------|----------------|-----------------|-------------|
  |Report Post                | ✔️           | ✔️                  | ✔️              |   ✔️             |             |
  |Report User                |             |                    |                |                 | *Not currently supported?*|
  |Remove Post From owned magazine | ✔️      |          ✔️         |      ✔️         |                 |             | 
  |Remove Post From 'random' magazine|  ✔️   |                    |                |                 |             |   
  |Block User from owned magazine |  ✔️      |            ✔️       |     ✔️          |                 |             |
  |Suspend User               | ✔️           |                    |                |                 |             |
  |Delete user account        | ✔️           |                    |                |                 |             |
  |Take over magazine         |             |                    |                |                 |             |
  |Ban / block federated Magazine |         |                    |                |                 | ???         |
  |Bulk remove content by a user |      ✔️   |                    |                |  \*          |*\* Users can block other users*|
  |Defederate                    |      ✔️   |              |          |  \*        |*\* Users can block instancnes for themselves*|

